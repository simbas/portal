# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](http://keepachangelog.com/en/1.0.0/)
and this project adheres to [Semantic Versioning](http://semver.org/spec/v2.0.0.html).

## [Unreleased]

## 1.0.0

### Added
- Display notification as alert
- Deploy docker image to gitlab registry
- History application to see notifications about user changes
- Photo application to change the user photo
- Display the menu to switch pages
- Name application to change the user name
- Display the user name in the header
- Display the portal title
