FROM node:carbon-stretch as builder

WORKDIR /usr/src/app
ARG CHROME_BIN=chromium
ARG LANGUAGE=en

RUN apt-get -qqy update && apt-get -qqy install chromium

COPY package*.json ./

RUN npm install

COPY . .

RUN npm run lint && \
    npm run test -- --code-coverage --watch false --progress false && \
    npm run mutation && \
    npm run e2e && \
    npm run screenshot && \
    npm run build

FROM nginx:1.13.10

RUN sed -i '11i try_files $uri$args $uri$args/ /index.html;' /etc/nginx/conf.d/default.conf
COPY --from=builder /usr/src/app/dist /usr/share/nginx/html
