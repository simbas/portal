Portal
---

## Usage

### From gitlab registry

```
docker run --rm -p 8080:80 registry.gitlab.com/simbas/portal:latest
```

### From source code

```
docker build -t portal .
docker run --rm -p 8080:80 portal
open http://localhost:8080
```

## Features

### Display the portal title and the user name

![header](./screenshots/header-chrome-800x600-dpr-1.png)

### Display the portal menu

![menu](./screenshots/menu-chrome-800x600-dpr-1.png)

### Application to update user name

![name](./screenshots/name-chrome-800x600-dpr-1.png)

### Application to update user photo

![photo](./screenshots/photo-chrome-800x600-dpr-1.png)

### Application to see notifications about user changes

![history](./screenshots/history-chrome-800x600-dpr-1.png)

### Display the notification as closable alert

![alert](./screenshots/alert-chrome-800x600-dpr-1.png)


## Conception

We have to implement a skeleton of a portal as a single page application with the following features:
- lifecycle of an application
- menu to access to the applications
- app layout
- 1 app to change the user name
- 1 app to upload a user photo
- 1 app to check the history of notification

We must focus on:
- the model and structure of the data
- the packaging of the application and the lifecycle of the application
- the event bubbling
- the scalability of the portal

We must use:
- Javascript or Typescript
- Any library or framework
- component-based architecture

### Framework

Angular has been written with scalability in mind. It seems a good choice for this application.

### State management

`@ngrx` is a popular state manager, it provides:
- scalability
- tooling
- consistency
