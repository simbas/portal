// Protractor configuration file, see link for more information
// https://github.com/angular/protractor/blob/master/lib/config.ts

const { SpecReporter } = require('jasmine-spec-reporter');
const PixDiff = require('pix-diff');

exports.config = {
  allScriptsTimeout: 11000,
  specs: ['./protractor/**/*.e2e-spec.ts'],
  capabilities: {
    'browserName': 'chrome',
    chromeOptions: {
      args: ["--headless", "--no-sandbox"]
    }
  },
  directConnect: true,
  baseUrl: 'http://localhost:4200/',
  framework: 'jasmine',
  jasmineNodeOpts: {
    showColors: true,
    defaultTimeoutInterval: 30000,
    print: function() {}
  },
  onPrepare() {
    browser.pixDiff = new PixDiff({
      basePath: './screenshots/',
      diffPath: './screenshots/',
      baseline: true
    });
    require('ts-node').register({
      project: 'protractor/tsconfig.protractor.json'
    });
    jasmine.getEnv().addReporter(new SpecReporter({ spec: { displayStacktrace: true } }));
  }
};
