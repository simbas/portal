import { by, element } from 'protractor';

export class Alert {
  getAlertContainer() {
    return element(by.css('portal-root portal-alert'));
  }
  getAlerts() {
    return element.all(by.css('portal-root .alert'));
  }
}
