import { browser } from 'protractor';
import * as PixDiff from 'pix-diff';
import { Alert } from './alert.po';
import { NameApp } from './name.po';

describe('alert', () => {
  let alert: Alert;
  let nameApp: NameApp;

  beforeEach(() => {
    browser.get('/');
    alert = new Alert();
    nameApp = new NameApp();
    nameApp.navigateTo();
  });

  it('should display the alert design', () => {
    nameApp.changeName('toto');
    browser.pixDiff.saveRegion(alert.getAlertContainer(), 'latest-alert');
    browser.pixDiff.checkRegion(alert.getAlertContainer(), 'alert')
      .then(result => expect(result.code).toEqual(PixDiff.RESULT_IDENTICAL));
  });
});
