import { Header } from './header.po';
import { browser } from 'protractor';

describe('header', () => {
  let header: Header;

  beforeEach(() => {
    browser.get('/');
    header = new Header();
  });

  it(`should display the app title 'Portal'`, () => {
    expect(header.getTitle().getText()).toEqual('Portal');
  });
  it(`should display the user name 'John Doe'`, () => {
    expect(header.getName().getText()).toEqual('John Doe');
  });
});
