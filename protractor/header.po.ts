import { by, element } from 'protractor';

export class Header {
  getTitle() {
    return element(by.css('portal-root .title'));
  }

  getName() {
    return element(by.css('portal-root .name'));
  }

  getHeader() {
    return element(by.css('portal-root header'));
  }
}
