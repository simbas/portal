import { Header } from './header.po';
import { browser } from 'protractor';
import * as PixDiff from 'pix-diff';

describe('header', () => {
  let header: Header;

  beforeEach(() => {
    browser.get('/');
    header = new Header();
  });

  it('should display the header design', () => {
    browser.pixDiff.saveRegion(header.getHeader(), 'latest-header');
    browser.pixDiff.checkRegion(header.getHeader(), 'header')
      .then(result => expect(result.code).toEqual(PixDiff.RESULT_IDENTICAL));
  });
});
