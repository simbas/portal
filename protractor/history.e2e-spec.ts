import { HistoryApp } from './history.po';
import { browser } from 'protractor';
import { NameApp } from './name.po';

describe('history app', () => {
  let historyApp: HistoryApp;
  let nameApp: NameApp;

  beforeEach(() => {
    browser.get('/');
    historyApp = new HistoryApp();
    nameApp = new NameApp();
    historyApp.navigateTo();
  });
  it(`should activate the history-app entry in the menu`, async() => {
    expect(historyApp.menu.getHistoryAppLink().getAttribute('class')).toMatch('active');
  });
  it(`should display 3 notifications after 3 name updates`, async () => {
    nameApp.navigateTo();
    nameApp.changeName('toto');
    nameApp.changeName('titi');
    nameApp.changeName('jojo');
    historyApp.navigateTo();
    expect(historyApp.getNotifications().count()).toBe(3);
  });
});
