import { browser, by, element } from 'protractor';
import { Header } from './header.po';
import { Menu } from './menu.po';

export class HistoryApp {
  header = new Header();
  menu = new Menu();

  navigateTo() {
    return this.menu.getHistoryAppLink().click();
  }

  getApp() {
    return element(by.css('portal-root .app'));
  }

  getNotifications() {
    return element.all(by.css('portal-root clr-dg-row'));
  }
}
