import { HistoryApp } from './history.po';
import * as PixDiff from 'pix-diff';
import { browser } from 'protractor';
import { NameApp } from './name.po';

describe('history', () => {
  let historyApp: HistoryApp;
  let nameApp: NameApp;

  beforeEach(() => {
    browser.get('/');
    historyApp = new HistoryApp();
    nameApp = new NameApp();
  });

  it('should display the history app design', () => {
    nameApp.navigateTo();
    nameApp.changeName('toto');
    nameApp.changeName('titi');
    nameApp.changeName('jojo');
    historyApp.navigateTo();
    browser.pixDiff.saveRegion(historyApp.getApp(), 'latest-history');
    browser.pixDiff.checkRegion(historyApp.getApp(), 'history')
      .then(result => expect(result.code).toEqual(PixDiff.RESULT_IDENTICAL));
  });
});
