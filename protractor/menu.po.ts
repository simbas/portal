import { by, element } from 'protractor';

export class Menu {
  getMenu() {
    return element(by.css('portal-root .sidenav'));
  }

  getNameAppLink() {
    return element(by.css('portal-root .name-link'));
  }

  getPhotoAppLink() {
    return element(by.css('portal-root .photo-link'));
  }

  getHistoryAppLink() {
    return element(by.css('portal-root .history-link'));
  }
}
