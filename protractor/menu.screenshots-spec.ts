import { Menu } from './menu.po';
import { browser } from 'protractor';
import * as PixDiff from 'pix-diff';

describe('menu', () => {
  let menu: Menu;

  beforeEach(() => {
    browser.get('/');
    menu = new Menu();
  });

  it('should display the menu design', () => {
    browser.pixDiff.saveRegion(menu.getMenu(), 'latest-menu');
    browser.pixDiff.checkRegion(menu.getMenu(), 'menu')
      .then(result => expect(result.code).toEqual(PixDiff.RESULT_IDENTICAL));
  });
});
