import { NameApp } from './name.po';
import { browser } from 'protractor';

describe('name app', () => {
  let nameApp: NameApp;

  beforeEach(() => {
    browser.get('/');
    nameApp = new NameApp();
    nameApp.navigateTo();
  });
  it(`should activate the name-app entry in the menu`, async() => {
    expect(nameApp.menu.getNameAppLink().getAttribute('class')).toMatch('active');
  });
  it(`should change the user name to 'toto' when submitting 'toto'`, async () => {
    nameApp.changeName('toto');
    expect(nameApp.header.getName().getText()).toEqual('toto');
  });
  it(`should display an alert when submitting 'toto'`, async () => {
    expect(nameApp.alert.getAlerts().count()).toBe(0);
    nameApp.changeName('toto');
    expect(nameApp.alert.getAlerts().count()).toBe(1);
  });
});
