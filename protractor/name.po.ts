import { by, element } from 'protractor';
import { Header } from './header.po';
import { Menu } from './menu.po';
import { Alert } from './alert.po';

export class NameApp {
  header = new Header();
  menu = new Menu();
  alert = new Alert();

  navigateTo() {
    return this.menu.getNameAppLink().click();
  }

  getApp() {
    return element(by.css('portal-root .app'));
  }

  getNameInput() {
    return element(by.css('portal-root .change-name'));
  }

  getSubmitButton() {
    return element(by.css('portal-root .submit'));
  }

  changeName(newName: string) {
    this.getNameInput().clear();
    this.getNameInput().sendKeys(newName);
    this.getSubmitButton().click();
  }
}
