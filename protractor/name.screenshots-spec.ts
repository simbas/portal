import { NameApp } from './name.po';
import { browser } from 'protractor';
import * as PixDiff from 'pix-diff';

describe('name app', () => {
  let nameApp: NameApp;

  beforeEach(() => {
    browser.get('/');
    nameApp = new NameApp();
    nameApp.navigateTo();
  });

  it('should display the name app design', () => {
    browser.pixDiff.saveRegion(nameApp.getApp(), 'latest-name');
    browser.pixDiff.checkRegion(nameApp.getApp(), 'name')
      .then(result => expect(result.code).toEqual(PixDiff.RESULT_IDENTICAL));
  });
});
