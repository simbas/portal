import { PhotoApp } from './photo.po';
import { browser } from 'protractor';

describe('photo app', () => {
  let photoApp: PhotoApp;

  beforeEach(() => {
    browser.get('/');
    photoApp = new PhotoApp();
    photoApp.navigateTo();
  });
  it(`should activate the photo-app entry in the menu`, async() => {
    expect(photoApp.menu.getPhotoAppLink().getAttribute('class')).toMatch('active');
  });
  it(`should display the current photo of the user`, async () => {
    expect(photoApp.getPhoto().isDisplayed()).toBeTruthy();
  });
  it(`should display the input to change the photo of the user`, async () => {
    expect(photoApp.getPhotoInput().isDisplayed()).toBeTruthy();
  });
});
