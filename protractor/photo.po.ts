import { by, element } from 'protractor';
import { Header } from './header.po';
import { Menu } from './menu.po';

export class PhotoApp {
  header = new Header();
  menu = new Menu();

  navigateTo() {
    return this.menu.getPhotoAppLink().click();
  }

  getApp() {
    return element(by.css('portal-root .app'));
  }

  getPhoto() {
    return element(by.css('portal-root .photo'));
  }

  getPhotoInput() {
    return element(by.css('portal-root .change-photo'));
  }
}
