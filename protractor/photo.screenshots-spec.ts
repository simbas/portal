import { PhotoApp } from './photo.po';
import { browser } from 'protractor';
import * as PixDiff from 'pix-diff';

describe('photo app', () => {
  let photoApp: PhotoApp;

  beforeEach(() => {
    browser.get('/');
    photoApp = new PhotoApp();
    photoApp.navigateTo();
  });

  it('should display the photo app design', () => {
    browser.pixDiff.saveRegion(photoApp.getApp(), 'latest-photo');
    browser.pixDiff.checkRegion(photoApp.getApp(), 'photo')
      .then(result => expect(result.code).toEqual(PixDiff.RESULT_IDENTICAL));
  });
});
