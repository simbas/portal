import { Component, EventEmitter, Input, Output } from '@angular/core';
import { Notification } from '../notification/notification.model';

@Component({
  selector: 'portal-alert',
  templateUrl: './alert.component.html'
})
export class AlertComponent {
  @Input() notifications: Array<Notification>;
  @Output() closeNotification = new EventEmitter<string>();
}
