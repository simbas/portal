import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

const routes: Routes = [{
  path: 'name',
  loadChildren: 'app/name-app/name-app.module#NameAppModule'
}, {
  path: 'photo',
  loadChildren: 'app/photo-app/photo-app.module#PhotoAppModule'
}, {
  path: 'history',
  loadChildren: 'app/history-app/history-app.module#HistoryAppModule'
}, {
  path: '**',
  redirectTo: '/name/edit'
}];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {
}
