import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { AppComponent } from './app.component';
import { Store, StoreModule } from '@ngrx/store';
import { reducer as userReducer, User, userInitialState } from './user/user.reducer';
import { ChangeName } from './user/user.actions';
import { Component, EventEmitter, Input, Output } from '@angular/core';
import { reducer as notificationReducer } from './notification/notification.reducer';
import { Notification } from './notification/notification.model';
import { By } from '@angular/platform-browser';
import { CloseNotification } from './notification/notification.actions';

@Component({
  selector: 'portal-header',
  template: ''
})
class HeaderComponent {
  @Input() name: string;
}

@Component({
  selector: 'portal-alert',
  template: ''
})
class AlertComponent {
  @Input() notifications: Array<Notification>;
  @Output() closeNotification = new EventEmitter<string>();
}

describe('AppComponent', () => {
  let component: AppComponent;
  let fixture: ComponentFixture<AppComponent>;
  let store: Store<User>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        RouterTestingModule,
        StoreModule.forRoot({
          user: userReducer,
          notification: notificationReducer
        })
      ],
      schemas: [],
      declarations: [AppComponent, HeaderComponent, AlertComponent],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AppComponent);
    component = fixture.componentInstance;
    store = TestBed.get(Store);

    spyOn(store, 'dispatch').and.callThrough();
    fixture.detectChanges();
  });
  it(`should be selected with 'portal-root'`, async(() => {
    const metadata = AppComponent['__annotations__'][0];
    expect(metadata.selector).toBe('portal-root');
  }));
  it(`should observe the store for 'user.name' changes`, async(() => {
    component.name.take(1).subscribe((name) => expect(name).toBe('John Doe'));
    store.dispatch(new ChangeName('titi'));
    component.name.take(1).subscribe((name) => expect(name).toBe('titi'));
  }));
  it(`should give the user name to the HeaderComponent`, async(() => {
    const headerComponent: HeaderComponent = fixture.debugElement.query(By.directive(HeaderComponent)).componentInstance;
    expect(headerComponent.name).toBe(userInitialState.name);
    store.dispatch(new ChangeName('toto'));
    fixture.detectChanges();
    expect(headerComponent.name).toBe('toto');
  }));
  it(`should observe the store for 'notification' changes`, async(() => {
    component.notifications.take(1).subscribe((notifications) => expect(notifications.length).toBe(0));
    store.dispatch(new ChangeName('toto'));
    component.notifications.take(1).subscribe((notifications) => expect(notifications.length).toBe(1));
  }));
  it(`should give the notifications to AlertComponent`, async(() => {
    const alertComponent: AlertComponent = fixture.debugElement.query(By.directive(AlertComponent)).componentInstance;
    expect(alertComponent.notifications.length).toBe(0);
    store.dispatch(new ChangeName('toto'));
    fixture.detectChanges();
    expect(alertComponent.notifications.length).toBe(1);
  }));
  it(`should dispatch a CloseNotification action (with '0' payload) when AlertComponent emit '0'`, () => {
    const alertComponent: AlertComponent = fixture.debugElement.query(By.directive(AlertComponent)).componentInstance;
    alertComponent.closeNotification.emit('0');
    expect(store.dispatch).toHaveBeenCalledWith(new CloseNotification('0'));
  });
});
