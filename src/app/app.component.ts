import { Component } from '@angular/core';
import { select, Store } from '@ngrx/store';
import { Observable } from 'rxjs/Observable';
import { selectUserName } from './user/user.selectors';
import { AppState } from './app.state';
import { Notification } from './notification/notification.model';
import { selectAllNotifications } from './notification/notification.selectors';
import { CloseNotification } from './notification/notification.actions';

@Component({
  selector: 'portal-root',
  templateUrl: './app.component.html',
})
export class AppComponent {
  name: Observable<string>;
  notifications: Observable<Array<Notification>>;

  constructor(private store: Store<AppState>) {
    this.name = this.store.pipe(select(selectUserName));
    this.notifications = this.store.pipe(select(selectAllNotifications));
  }

  closeNotification(id: string) {
    this.store.dispatch(new CloseNotification(id));
  }
}
