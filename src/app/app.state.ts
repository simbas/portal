import { User } from './user/user.reducer';
import { EntityState } from '@ngrx/entity';

export interface AppState {
  user: User;
  notification: EntityState<Notification>;
}
