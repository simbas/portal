import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { HeaderComponent } from './header.component';
import { NO_ERRORS_SCHEMA } from '@angular/core';

describe('HeaderComponent', () => {
  let component: HeaderComponent;
  let fixture: ComponentFixture<HeaderComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [HeaderComponent],
      schemas: [NO_ERRORS_SCHEMA]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HeaderComponent);
    component = fixture.componentInstance;
    component.name = 'John';
    fixture.detectChanges();
  });
  it(`should be selected with 'portal-header'`, async(() => {
    const metadata = HeaderComponent['__annotations__'][0];
    expect(metadata.selector).toBe('portal-header');
  }));
  it('should render the portal title in a .title tag', async(() => {
    const compiled = fixture.debugElement.nativeElement;
    expect(compiled.querySelector('.title').textContent).toContain('Portal');
  }));

  it(`should display 'John' (the user name) in a .name tag`, () => {
    const compiled = fixture.debugElement.nativeElement;
    expect(compiled.querySelector('.name').textContent).toContain('John');
  });
});
