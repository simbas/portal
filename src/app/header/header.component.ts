import { Component, Input } from '@angular/core';

@Component({
  selector: 'portal-header',
  templateUrl: './header.component.html'
})
export class HeaderComponent {
  @Input() name: string;
}
