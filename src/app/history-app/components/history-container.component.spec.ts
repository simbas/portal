import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HistoryContainerComponent } from './history-container.component';
import { Store, StoreModule } from '@ngrx/store';
import { Component, Input } from '@angular/core';
import { Notification } from '../../notification/notification.model';
import { ChangeName } from '../../user/user.actions';
import { AppState } from '../../app.state';
import { By } from '@angular/platform-browser';
import { reducer } from '../../notification/notification.reducer';
import { notificationFeature } from '../../notification/notification.module';


@Component({
  selector: 'portal-history',
  template: ''
})
class HistoryComponent {
  @Input() notifications: Array<Notification>;
}


describe('HistoryContainerComponent', () => {
  let component: HistoryContainerComponent;
  let fixture: ComponentFixture<HistoryContainerComponent>;
  let store: Store<AppState>;

  beforeEach(async () => {
    TestBed.configureTestingModule({
      imports: [StoreModule.forRoot({}), StoreModule.forFeature(notificationFeature, reducer)],
      declarations: [HistoryContainerComponent, HistoryComponent]
    });

    await TestBed.compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(HistoryContainerComponent);
    component = fixture.componentInstance;
    store = TestBed.get(Store);

    spyOn(store, 'dispatch').and.callThrough();
    fixture.detectChanges();
  });
  it(`should be selected with 'portal-history-app'`, async(() => {
    const metadata = HistoryContainerComponent['__annotations__'][0];
    expect(metadata.selector).toBe('portal-history-app');
  }));
  it(`should observe the store for 'notification' changes`, async(() => {
    component.notifications.take(1).subscribe((notifications) => expect(notifications.length).toBe(0));
    store.dispatch(new ChangeName('toto'));
    component.notifications.take(1).subscribe((notifications) => expect(notifications.length).toBe(1));
  }));
  it(`should give the notifications to HistoryComponent`, async(() => {
    const historyComponent: HistoryComponent = fixture.debugElement.query(By.directive(HistoryComponent)).componentInstance;
    expect(historyComponent.notifications.length).toBe(0);
    store.dispatch(new ChangeName('toto'));
    fixture.detectChanges();
    expect(historyComponent.notifications.length).toBe(1);
  }));
});
