import { Component } from '@angular/core';
import { select, Store } from '@ngrx/store';
import { AppState } from '../../app.state';
import { selectAllNotifications } from '../../notification/notification.selectors';
import { Observable } from 'rxjs/Observable';
import { Notification } from '../../notification/notification.model';
@Component({
  selector: 'portal-history-app',
  template: '<portal-history [notifications]="notifications | async"></portal-history>'
})
export class HistoryContainerComponent {
  notifications: Observable<Array<Notification>>;

  constructor(private store: Store<AppState>) {
    this.notifications = this.store.pipe(select(selectAllNotifications));
  }
}
