import { Component, Input } from '@angular/core';
import { Notification } from '../../notification/notification.model';

@Component({
  selector: 'portal-history',
  templateUrl: './history.component.html'
})
export class HistoryComponent {
    @Input() notifications: Array<Notification>;
}
