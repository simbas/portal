import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HistoryContainerComponent } from './components/history-container.component';
import { HistoryComponent } from './components/history.component';
import { HistoryAppRoutingModule } from './history-app-routing.module';
import { ClarityModule } from '@clr/angular';

@NgModule({
  imports: [
    CommonModule,
    HistoryAppRoutingModule,
    ClarityModule
  ],
  declarations: [HistoryContainerComponent, HistoryComponent]
})
export class HistoryAppModule { }
