import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NameContainerComponent } from './name-container.component';
import { Store, StoreModule } from '@ngrx/store';
import { reducer, User, userInitialState } from '../../user/user.reducer';
import { ChangeName } from '../../user/user.actions';
import { By } from '@angular/platform-browser';
import 'rxjs/add/operator/take';
import { Component, EventEmitter, Input, Output } from '@angular/core';
import { userFeature } from '../../user/user.module';


@Component({
  selector: 'portal-name',
  template: ''
})
class NameComponent {
  @Input() name: string;
  @Output() changeName = new EventEmitter<string>();
}

describe('NameAppComponent', () => {
  let component: NameContainerComponent;
  let fixture: ComponentFixture<NameContainerComponent>;
  let store: Store<User>;

  beforeEach(async () => {
    TestBed.configureTestingModule({
      imports: [StoreModule.forRoot({}), StoreModule.forFeature(userFeature, reducer)],
      declarations: [NameContainerComponent, NameComponent]
    });

    await TestBed.compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(NameContainerComponent);
    component = fixture.componentInstance;
    store = TestBed.get(Store);

    spyOn(store, 'dispatch').and.callThrough();
    fixture.detectChanges();
  });
  it(`should be selected with 'portal-name-app'`, async(() => {
    const metadata = NameContainerComponent['__annotations__'][0];
    expect(metadata.selector).toBe('portal-name-app');
  }));
  it(`should contains a NameComponent`, async(() => {
    expect(fixture.debugElement.queryAll(By.directive(NameComponent)).length).toBe(1);
  }));
  it(`should observe the store for 'user.name' changes`, async(() => {
    component.name.take(1).subscribe((name) => expect(name).toBe(userInitialState.name));
    store.dispatch(new ChangeName('toto'));
    component.name.take(1).subscribe((name) => expect(name).toBe('toto'));
  }));
  it(`should give the user name to the NameComponent`, async(() => {
    const nameComponent: NameComponent = fixture.debugElement.query(By.directive(NameComponent)).componentInstance;
    expect(nameComponent.name).toBe(userInitialState.name);
    store.dispatch(new ChangeName('toto'));
    fixture.detectChanges();
    expect(nameComponent.name).toBe('toto');
  }));
  it(`should dispatch a ChangeName action (with 'toto' payload) when NameComponent emit 'toto'`, () => {
    const nameComponent: NameComponent = fixture.debugElement.query(By.directive(NameComponent)).componentInstance;
    nameComponent.changeName.emit('toto');
    expect(store.dispatch).toHaveBeenCalledWith(new ChangeName('toto'));
  });
});
