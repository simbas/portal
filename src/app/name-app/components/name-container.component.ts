import { Component } from '@angular/core';
import { select, Store } from '@ngrx/store';
import { selectUserName } from '../../user/user.selectors';
import { Observable } from 'rxjs/Observable';
import { ChangeName } from '../../user/user.actions';
import { AppState } from '../../app.state';

@Component({
  selector: 'portal-name-app',
  template: `
    <portal-name [name]="name | async" (changeName)="this.changeName($event)"></portal-name>
  `
})
export class NameContainerComponent {
  name: Observable<string>;

  constructor(private store: Store<AppState>) {
    this.name = this.store.pipe(select(selectUserName));
  }

  changeName(name: string) {
    this.store.dispatch(new ChangeName(name));
  }
}
