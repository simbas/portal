import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NameComponent } from './name.component';
import { ReactiveFormsModule } from '@angular/forms';

describe('NameComponent', () => {
  let component: NameComponent;
  let fixture: ComponentFixture<NameComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [ReactiveFormsModule],
      declarations: [NameComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NameComponent);
    component = fixture.componentInstance;
    component.name = 'toto';
    fixture.detectChanges();
  });
  it(`should be selected with 'portal-name'`, async(() => {
    const metadata = NameComponent['__annotations__'][0];
    expect(metadata.selector).toBe('portal-name');
  }));
  it(`should emit the value of the name input on submit`, async(() => {
    spyOn(component.changeName, 'emit');
    component.form.get('name').setValue('titi');
    component.onSubmit();
    expect(component.changeName.emit).toHaveBeenCalledWith('titi');
  }));
});
