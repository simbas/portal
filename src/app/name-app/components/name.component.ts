import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'portal-name',
  templateUrl: './name.component.html'
})
export class NameComponent implements OnInit {
  @Input() name: string;
  @Output() changeName = new EventEmitter<string>();
  form: FormGroup;

  ngOnInit() {
    this.form = new FormGroup({
      name: new FormControl(this.name, Validators.required)
    });
  }

  onSubmit() {
    this.changeName.emit((this.form.getRawValue() as { name: string }).name);
  }
}
