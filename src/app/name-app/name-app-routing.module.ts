import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { NameContainerComponent } from './components/name-container.component';

const routes: Routes = [
  {path: 'edit', component: NameContainerComponent}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class NameAppRoutingModule {
}
