import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NameContainerComponent } from './components/name-container.component';
import { NameComponent } from './components/name.component';
import { ReactiveFormsModule } from '@angular/forms';
import { NameAppRoutingModule } from './name-app-routing.module';

@NgModule({
  imports: [
    CommonModule,
    ReactiveFormsModule,
    NameAppRoutingModule
  ],
  declarations: [NameContainerComponent, NameComponent]
})
export class NameAppModule {
}
