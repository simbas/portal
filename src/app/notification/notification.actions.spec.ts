import { ChangePhoto } from '../user/user.actions';
import { CloseNotification, isCloseNotificationAction, NotificationActionTypes } from './notification.actions';

describe('Notification Actions', () => {
  describe('NotificationActionTypes', () => {
    describe('CloseNotification', () => {
      it(` should be '[Notification] Close Notification'`, () => {
        expect(NotificationActionTypes.CloseNotification).toBe('[Notification] Close Notification');
      });
    });
  });
  describe('isCloseNotificationAction', () => {
    it('should return true when giving a CloseNotification action', () => {
      expect(isCloseNotificationAction(new CloseNotification('0'))).toBeTruthy();
    });
    it('should return false when giving a ChangePhoto action', () => {
      expect(isCloseNotificationAction(new ChangePhoto('whatever'))).toBeFalsy();
    });
  });
});
