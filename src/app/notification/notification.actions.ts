import { Action } from '@ngrx/store';

export enum NotificationActionTypes {
  CloseNotification = '[Notification] Close Notification'
}

export class CloseNotification implements Action {
  readonly type = NotificationActionTypes.CloseNotification;

  constructor(public payload: string) {}
}

export function isCloseNotificationAction(action: Action): action is CloseNotification {
  return action.type === NotificationActionTypes.CloseNotification;
}
