import {
  changeNameNotification,
  changePhotoNotification,
  isChangeNameNotification,
  isChangePhotoNotification,
  NotificationTypes
} from './notification.model';

describe('Notification Model', () => {
  describe('NotificationTypes', () => {
    describe('ChangeName', () => {
      it(` should be '[Notification] Change Name'`, () => {
        expect(NotificationTypes.ChangeName).toEqual('[Notification] Change Name');
      });
    });
    describe('ChangePhoto', () => {
      it(` should be '[Notification] Change Photo'`, () => {
        expect(NotificationTypes.ChangePhoto).toEqual('[Notification] Change Photo');
      });
    });
  });
  describe('changeNameNotification', () => {
    it('create an asAlert ChangeNameNotification if asAlert argument not supplied', () => {
      expect(changeNameNotification('0', 'whatever').asAlert).toBeTruthy();
    });
  });
  describe('changePhotoNotification', () => {
    it('create an asAlert ChangePhotoNotification if asAlert argument not supplied', () => {
      expect(changePhotoNotification('0').asAlert).toBeTruthy();
    });
  });
  describe('isChangeNameNotification', () => {
    it('should return true when giving a ChangeNameNotification', () => {
      expect(isChangeNameNotification(changeNameNotification('0', 'whatever'))).toBeTruthy();
    });
    it('should return true when giving a closed ChangeNameNotification', () => {
      expect(isChangeNameNotification(changeNameNotification('0', 'whatever', false))).toBeTruthy();
    });
    it('should return false when giving a ChangePhotoNotification', () => {
      expect(isChangeNameNotification(changePhotoNotification('0'))).toBeFalsy();
    });
    it('should return false when giving a closed ChangePhotoNotification', () => {
      expect(isChangeNameNotification(changePhotoNotification('0', false))).toBeFalsy();
    });
  });
  describe('isChangePhotoNotification', () => {
    it('should return true when giving a ChangePhotoNotification', () => {
      expect(isChangePhotoNotification(changePhotoNotification('0'))).toBeTruthy();
    });
    it('should return true when giving a closed ChangePhotoNotification', () => {
      expect(isChangePhotoNotification(changePhotoNotification('0', false))).toBeTruthy();
    });
    it('should return false when giving a ChangeNameNotification', () => {
      expect(isChangePhotoNotification(changeNameNotification('0', 'whatever'))).toBeFalsy();
    });
    it('should return false when giving a closed ChangeNameNotification', () => {
      expect(isChangePhotoNotification(changeNameNotification('0', 'whatever', false))).toBeFalsy();
    });
  });
});
