export enum NotificationTypes {
  ChangeName = '[Notification] Change Name',
  ChangePhoto = '[Notification] Change Photo'
}

interface BaseNotification {
  id: string;
  asAlert: boolean;
}

export interface ChangeNameNotification extends BaseNotification {
  type: NotificationTypes.ChangeName;
  name: string;
}
export interface ChangePhotoNotification extends  BaseNotification {
  type: NotificationTypes.ChangePhoto;
}

export function changeNameNotification(id: string, name: string, asAlert: boolean = true): ChangeNameNotification {
  return {id, name, asAlert, type: NotificationTypes.ChangeName};
}

export function changePhotoNotification(id: string, asAlert: boolean = true): ChangePhotoNotification {
  return {id, asAlert, type: NotificationTypes.ChangePhoto};
}

export type Notification = ChangeNameNotification | ChangePhotoNotification;

export function isChangeNameNotification(notification: Notification): notification is ChangeNameNotification {
  return notification.type === NotificationTypes.ChangeName;
}

export function isChangePhotoNotification(notification: Notification): notification is ChangePhotoNotification {
  return notification.type === NotificationTypes.ChangePhoto;
}
