import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { StoreModule } from '@ngrx/store';
import * as fromNotification from './notification.reducer';

export const notificationFeature = 'notification';

@NgModule({
  imports: [
    CommonModule,
    StoreModule.forFeature(notificationFeature, fromNotification.reducer)
  ],
  declarations: []
})
export class NotificationModule { }
