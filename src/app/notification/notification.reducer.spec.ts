import { initialState, reducer, sortByDescendingIds } from './notification.reducer';
import { changeNameNotification, changePhotoNotification } from './notification.model';
import { ChangeName, ChangePhoto } from '../user/user.actions';
import { CloseNotification } from './notification.actions';

describe('Notification Reducer', () => {
  describe('unknown action', () => {
    it('should return the initial state', () => {
      const action = {} as any;

      const result = reducer(initialState, action);

      expect(result).toBe(initialState);
    });
  });
  describe('initial state', () => {
    it(`should be an empty EntityState`, () => {
      const action = {} as any;
      const state = reducer(undefined, action);
      expect(state.ids.length).toBe(0);
    });
  });
  describe('ChangeName action', () => {
    it(`should adds a ChangeNameNotification`, () => {
      const newState = reducer(initialState, new ChangeName('toto'));
      expect(newState.ids.length).toBe(1);
      expect(newState.entities[0]).toEqual(changeNameNotification('0', 'toto'));
    });
  });
  describe('ChangePhoto action', () => {
    it(`should adds a ChangePhotoNotification`, () => {
      const newState = reducer(initialState, new ChangePhoto('toto'));
      expect(newState.ids.length).toBe(1);
      expect(newState.entities[0]).toEqual(changePhotoNotification('0'));
    });
  });
  describe('CloseNotification action', () => {
    it(`should close the given Notification`, () => {
      const firstState = reducer(initialState, new ChangePhoto('toto'));
      const secondState = reducer(firstState, new CloseNotification('0'));
      expect(secondState.entities[0]).toEqual(changePhotoNotification('0', false));
    });
  });
  describe('sortByDescendingIds', () => {
    it(`it should return 1 for {id: '0'}, {id: '1'}`, () => {
      const id0 = {id: '0'};
      const id1 = {id: '1'};
      expect(sortByDescendingIds(id0, id1)).toBe(1);
    });
    it(`it should return -1 for {id: '1'}, {id: '0'}`, () => {
      const id0 = {id: '0'};
      const id1 = {id: '1'};
      expect(sortByDescendingIds(id1, id0)).toBe(-1);
    });
  });
});
