import { createEntityAdapter, EntityAdapter, EntityState } from '@ngrx/entity';
import { changeNameNotification, changePhotoNotification, Notification } from './notification.model';
import { isChangeNameAction, isChangePhotoAction } from '../user/user.actions';
import { Action } from '@ngrx/store';
import { isCloseNotificationAction } from './notification.actions';

export interface State extends EntityState<Notification> {
}

export function sortByDescendingIds(a: {id: string}, b: {id: string}) {
  return parseInt(b.id, 10) - parseInt(a.id, 10);
}

export const adapter: EntityAdapter<Notification> = createEntityAdapter<Notification>({sortComparer: sortByDescendingIds});

export const initialState: State = adapter.getInitialState();

export function reducer(state = initialState, action: Action): State {
  const nextId = String(state.ids.length);
  if (isCloseNotificationAction(action)) {
    return adapter.updateOne({id: action.payload, changes: {asAlert: false}}, state);
  }
  if (isChangeNameAction(action)) {
    return adapter.addOne(changeNameNotification(nextId, action.payload), state);
  }
  if (isChangePhotoAction(action)) {
    return adapter.addOne(changePhotoNotification(nextId), state);
  }
  return state;
}
