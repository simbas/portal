import { adapter } from './notification.reducer';
import { createFeatureSelector, createSelector } from '@ngrx/store';
import { notificationFeature } from './notification.module';

const {selectAll} = adapter.getSelectors();
export const selectNotifications = createFeatureSelector(notificationFeature);
export const selectAllNotifications = createSelector(selectNotifications, selectAll);
