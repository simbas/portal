import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { Store, StoreModule } from '@ngrx/store';
import { reducer, User, userInitialState } from '../../user/user.reducer';
import { ChangePhoto } from '../../user/user.actions';
import { By, SafeUrl } from '@angular/platform-browser';
import 'rxjs/add/operator/take';
import { PhotoContainerComponent } from './photo-container.component';
import { Component, EventEmitter, Input, Output } from '@angular/core';
import { userFeature } from '../../user/user.module';


@Component({
  selector: 'portal-photo',
  template: ''
})
class PhotoComponent {
  @Input() photo: SafeUrl;
  @Output() changePhoto = new EventEmitter<SafeUrl>();
}

describe('PhotoAppComponent', () => {
  let component: PhotoContainerComponent;
  let fixture: ComponentFixture<PhotoContainerComponent>;
  let store: Store<User>;

  beforeEach(async () => {
    TestBed.configureTestingModule({
      imports: [StoreModule.forRoot({}), StoreModule.forFeature(userFeature, reducer)],
      declarations: [PhotoContainerComponent, PhotoComponent]
    });

    await TestBed.compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PhotoContainerComponent);
    component = fixture.componentInstance;
    store = TestBed.get(Store);

    spyOn(store, 'dispatch').and.callThrough();
    fixture.detectChanges();
  });
  it(`should be selected with 'portal-photo-app'`, async(() => {
    const metadata = PhotoContainerComponent['__annotations__'][0];
    expect(metadata.selector).toBe('portal-photo-app');
  }));
  it(`should contains a PhotoComponent`, async(() => {
    expect(fixture.debugElement.queryAll(By.directive(PhotoComponent)).length).toBe(1);
  }));
  it(`should observe the store for 'user.photo' changes`, async(() => {
    component.photo.take(1).subscribe((name) => expect(name).toBe(userInitialState.photo));
    store.dispatch(new ChangePhoto('titi'));
    component.photo.take(1).subscribe((name) => expect(name).toBe('titi'));
  }));
  it(`should dispatch a ChangePhoto action (with 'toto' payload) when calling ChangePhoto function with 'toto'`, () => {
    component.changePhoto('toto');
    expect(store.dispatch).toHaveBeenCalledWith(new ChangePhoto('toto'));
  });
  it(`should give the user photo to the PhotoComponent`, async(() => {
    const photoComponent: PhotoComponent = fixture.debugElement.query(By.directive(PhotoComponent)).componentInstance;
    expect(photoComponent.photo).toBe(userInitialState.photo);
    store.dispatch(new ChangePhoto('toto'));
    fixture.detectChanges();
    expect(photoComponent.photo).toBe('toto');
  }));
  it(`should dispatch a ChangePhoto action (with 'toto' payload) when PhotoComponent emit 'toto'`, () => {
    const photoComponent: PhotoComponent = fixture.debugElement.query(By.directive(PhotoComponent)).componentInstance;
    photoComponent.changePhoto.emit('toto');
    expect(store.dispatch).toHaveBeenCalledWith(new ChangePhoto('toto'));
  });
});
