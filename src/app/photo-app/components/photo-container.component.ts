import { Component } from '@angular/core';
import { select, Store } from '@ngrx/store';
import { Observable } from 'rxjs/Observable';
import { ChangePhoto } from '../../user/user.actions';
import { selectUserPhoto } from '../../user/user.selectors';
import { SafeUrl } from '@angular/platform-browser';
import { AppState } from '../../app.state';

@Component({
  selector: 'portal-photo-app',
  template: `
    <portal-photo [photo]="photo | async" (changePhoto)="this.changePhoto($event)"></portal-photo>
  `
})
export class PhotoContainerComponent {
  photo: Observable<SafeUrl>;

  constructor(private store: Store<AppState>) {
    this.photo = this.store.pipe(select(selectUserPhoto));
  }

  changePhoto(photo: SafeUrl) {
    this.store.dispatch(new ChangePhoto(photo));
  }
}
