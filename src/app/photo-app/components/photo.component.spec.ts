import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PhotoComponent } from './photo.component';
import { ReactiveFormsModule } from '@angular/forms';

describe('PhotoComponent', () => {
  let component: PhotoComponent;
  let fixture: ComponentFixture<PhotoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [ReactiveFormsModule],
      declarations: [PhotoComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PhotoComponent);
    component = fixture.componentInstance;
    component.photo = 'toto';
    fixture.detectChanges();
  });
  it(`should be selected with 'portal-photo'`, async(() => {
    const metadata = PhotoComponent['__annotations__'][0];
    expect(metadata.selector).toBe('portal-photo');
  }));
  it(`should emit a ChangePhoto value when photo input change`, async(() => {
    const event = {target: {files: [new File([''], 'img.png', {type: 'image/png'})]}};
    spyOn(component.changePhoto, 'emit');
    component.onInputChange(event);
    expect(component.changePhoto.emit).toHaveBeenCalled();
  }));
});
