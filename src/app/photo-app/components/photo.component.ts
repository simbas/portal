import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { DomSanitizer, SafeUrl } from '@angular/platform-browser';

@Component({
  selector: 'portal-photo',
  templateUrl: './photo.component.html'
})
export class PhotoComponent implements OnInit {
  @Input() photo: string;
  @Output() changePhoto = new EventEmitter<SafeUrl>();
  form: FormGroup;

  constructor(private sanitizer: DomSanitizer) {
  }

  ngOnInit() {
    this.form = new FormGroup({
      photo: new FormControl(null, Validators.required)
    });
  }

  onInputChange(event: {target: {files: Array<any>}}) {
    const file = event.target.files[0];
    const url = window.URL.createObjectURL(file);
    this.changePhoto.emit(this.sanitizer.bypassSecurityTrustUrl(url));
  }
}
