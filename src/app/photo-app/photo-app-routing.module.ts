import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { PhotoContainerComponent } from './components/photo-container.component';

const routes: Routes = [
  {path: 'edit', component: PhotoContainerComponent}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PhotoAppRoutingModule {
}
