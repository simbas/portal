import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule } from '@angular/forms';
import { PhotoAppRoutingModule } from './photo-app-routing.module';
import { PhotoContainerComponent } from './components/photo-container.component';
import { PhotoComponent } from './components/photo.component';

@NgModule({
  imports: [
    CommonModule,
    ReactiveFormsModule,
    PhotoAppRoutingModule
  ],
  declarations: [PhotoContainerComponent, PhotoComponent]
})
export class PhotoAppModule {
}
