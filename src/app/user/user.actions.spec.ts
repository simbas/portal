import { ChangeName, ChangePhoto, isChangeNameAction, isChangePhotoAction, UserActionTypes } from './user.actions';

describe('User Actions', () => {
  describe('UserActionTypes', () => {
    describe('ChangeName', () => {
      it(` should be '[User] Change Name'`, () => {
        expect(UserActionTypes.ChangeName).toBe('[User] Change Name');
      });
    });
    describe('ChangePhoto', () => {
      it(` should be '[User] Change Photo'`, () => {
        expect(UserActionTypes.ChangePhoto).toBe('[User] Change Photo');
      });
    });
  });
  describe('isChangeNameAction', () => {
    it('should return true when giving a ChangeName action', () => {
      expect(isChangeNameAction(new ChangeName('whatever'))).toBeTruthy();
    });
    it('should return false when giving a ChangePhoto action', () => {
      expect(isChangeNameAction(new ChangePhoto('whatever'))).toBeFalsy();
    });
  });
  describe('isChangePhotoAction', () => {
    it('should return true when giving a ChangePhoto action', () => {
      expect(isChangePhotoAction(new ChangePhoto('whatever'))).toBeTruthy();
    });
    it('should return false when giving a ChangeName action', () => {
      expect(isChangePhotoAction(new ChangeName('whatever'))).toBeFalsy();
    });
  });
});
