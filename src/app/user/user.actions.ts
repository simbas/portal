import { Action } from '@ngrx/store';
import { SafeUrl } from '@angular/platform-browser';

export enum UserActionTypes {
  ChangeName = '[User] Change Name',
  ChangePhoto = '[User] Change Photo'
}

export class ChangeName implements Action {
  readonly type = UserActionTypes.ChangeName;

  constructor(public payload: string) {
  }
}

export class ChangePhoto implements Action {
  readonly type = UserActionTypes.ChangePhoto;

  constructor(public payload: SafeUrl) {
  }
}

export function isChangeNameAction(action: Action): action is ChangeName {
  return action.type === UserActionTypes.ChangeName;
}

export function isChangePhotoAction(action: Action): action is ChangePhoto {
  return action.type === UserActionTypes.ChangePhoto;
}
