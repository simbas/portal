import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { StoreModule } from '@ngrx/store';
import { reducer } from './user.reducer';

export const userFeature = 'user';

@NgModule({
  imports: [
    CommonModule,
    StoreModule.forFeature(userFeature, reducer)
  ],
  declarations: []
})
export class UserModule {
}
