import { reducer, User } from './user.reducer';
import { ChangeName, ChangePhoto } from './user.actions';

const photo1 = 'photo1.png';
const photo2 = 'photo2.png';

const userNamedJohnDoeWithInitialPhoto: User = {name: 'John Doe', photo: '/assets/questionMark.png'};
const userNamedTotoWithPhoto1: User = {name: 'toto', photo: photo1};
const userNamedTotoWithPhoto2: User = {name: 'toto', photo: photo2};
const userNamedTitiWWithPhoto1: User = {name: 'titi', photo: photo1};

describe('User Reducer', () => {
  describe('unknown action', () => {
    it(`should return user named 'toto' with interrogation mark photo
      when giving user named 'toto' with interrogation mark photo`, () => {
      const action = {} as any;
      expect(reducer(userNamedTotoWithPhoto1, action)).toEqual(userNamedTotoWithPhoto1);
    });
  });
  describe('initial state', () => {
    it(`should be user named 'John Doe' with photo 1`, () => {
      const action = {} as any;
      expect(reducer(undefined, action)).toEqual(userNamedJohnDoeWithInitialPhoto);
    });
  });
  describe(`change name action`, () => {
    it(`should return user with name 'titi' when giving a user named 'toto' and a payload with 'titi'`, () => {
      expect(reducer(userNamedTotoWithPhoto1, new ChangeName('titi'))).toEqual(userNamedTitiWWithPhoto1);
    });
  });
  describe(`change photo action`, () => {
    it(`should return user named 'toto' with photo 2
      when giving a user named 'toto' with photo 1 and a payload with photo 2`, () => {
      expect(reducer(userNamedTotoWithPhoto1, new ChangePhoto(photo2))).toEqual(userNamedTotoWithPhoto2);
    });
  });
});
