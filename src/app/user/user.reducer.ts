import { isChangeNameAction, isChangePhotoAction } from './user.actions';
import { SafeUrl } from '@angular/platform-browser';
import { Action } from '@ngrx/store';

export interface User {
  name: string;
  photo: SafeUrl;
}

export const userInitialState: User = {
  name: 'John Doe',
  photo: '/assets/questionMark.png'
};

export function reducer(user: User = userInitialState, action: Action): User {
  if (isChangeNameAction(action)) {
    return {
      name: action.payload,
      photo: user.photo
    };
  }
  if (isChangePhotoAction(action)) {
    return {
      name: user.name,
      photo: action.payload
    };
  }
  return user;
}
