import { createFeatureSelector, createSelector } from '@ngrx/store';
import { User } from './user.reducer';
import { userFeature } from './user.module';

export const selectUser = createFeatureSelector<User>(userFeature);
export const selectUserName = createSelector(selectUser, (user: User) => user.name);
export const selectUserPhoto = createSelector(selectUser, (user: User) => user.photo);
